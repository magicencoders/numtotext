    var ones=['','один','два','три','четыре','пять','шесть','семь','восемь','девять'];
    var tens=['','','двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят','восемьдесят','девяносто'];
    var teens=['десять','одинадцать','двенадцать','тринадцать','четырнадцать','пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать'];


function toThousands(num){
    if (num === 1000){
        return "одна тысяча "+toHundreds(num%1000);
    }else if(num > 1000 ){
        return toHundreds(Math.floor(num/1000))+" тысяч "+toHundreds(num%1000);
    }else{
        return toHundreds(num);
    }
}

function toHundreds(num){

    if (num>99 && num <= 199){
        return "сто "+toTens(num%100);
    }else if(num >199 && num <= 299){
        return "двести "+toTens(num%100);
    }else if(num >299 && num <= 399){
        return "триста "+toTens(num%100);
    }else if(num >399 && num <= 499){
        return "четыреста "+toTens(num%100);
    }else if(num > 499 && num <= 599){
        return "пятсот "+toTens(num%100);
    }else if(num > 599 && num <= 699){
        return "шестсот "+toTens(num%100);
    }else if(num > 699 && num <= 799){
        return "семьсот "+toTens(num%100);
    }else if(num >799 && num <= 899){
    return "восемьсот "+toTens(num%100);
      }else if(num >899){
    return "девятьсот "+toTens(num%100);
    }else{
        return toTens(num);
    }
}

function toTens(num){
    if (num<10) return ones[num];
    else if (num>=10 && num<20) return teens[num-10];
    else{
        return tens[Math.floor(num/10)]+" "+ones[num%10];
    }
}

function convertNumber(num){
    if (num==0) return "ноль";
    else return toThousands(num);
}

function init(){
    var numbers=[1,2,3,4,7,10,12,202,299,399,400,555,666,9999,199,299,399,499,599,699,799,899,999];
    for (var i=0;i<numbers.length;i++ ){
        document.write(numbers[i]+": "+convertNumber(numbers[i])+"<br></br>");
    }
}

init();
